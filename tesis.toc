\select@language {spanish}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}
\contentsline {section}{\numberline {1.1}Objetivo General}{1}
\contentsline {section}{\numberline {1.2}Objetivos Espec\IeC {\'\i }ficos}{2}
\contentsline {chapter}{\numberline {2}Antecedentes Generales}{3}
\contentsline {section}{\numberline {2.1}Conocimientos B\IeC {\'a}sicos}{3}
\contentsline {subsection}{\numberline {2.1.1}Coprocesador Intel Xeon Phi}{3}
\contentsline {section}{\numberline {2.2}Espacios m\IeC {\'e}tricos}{4}
\contentsline {subsection}{\numberline {2.2.1}B\IeC {\'u}squedas por similitud}{5}
\contentsline {subsection}{\numberline {2.2.2}Indexaci'on}{6}
\contentsline {subsection}{\numberline {2.2.3}\emph {\IeC {\'I}ndices m\IeC {\'e}tricos}}{9}
\contentsline {subsubsection}{\numberline {2.2.3.1}Lista de Clusters (\emph {LC}) }{9}
\contentsline {section}{\numberline {2.3}Trabajo Relacionado}{11}
\contentsline {subsection}{\numberline {2.3.1}Trabajo relacionado en Sistemas multi-core}{11}
\contentsline {subsection}{\numberline {2.3.2}Trabajo Relacionado en GPU}{13}
\contentsline {chapter}{\numberline {3}Materiales y M\IeC {\'e}todos}{15}
\contentsline {section}{\numberline {3.1}Materiales}{15}
\contentsline {section}{\numberline {3.2}Metodolog\IeC {\'\i }a}{16}
\contentsline {subsection}{\numberline {3.2.1} Hip\IeC {\'o}tesis}{16}
\contentsline {section}{\numberline {3.3}Objetivos}{16}
\contentsline {section}{\numberline {3.4}Metodolog\IeC {\'\i }a}{16}
\contentsline {chapter}{\numberline {4}Programa de Trabajo}{19}
\contentsline {section}{\numberline {4.1}Plan de Trabajo}{19}
\contentsline {section}{Referencias}{20}
